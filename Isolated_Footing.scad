//added yz and xz projection views.

// The origin for this file is at the centre of the bottom surface of footing.
// length of footing is referred parallel to x-direction
// width of footing is referred parallel to y-direction
// support for unequal spacing between rebars, not added

//// INPUT STARTS HERE ////

//size of the column:
//assuming the column is at the center of the footing in both x and y axis.
//height of column, from top of footing (including tapered portion, if any.
height_of_column = 2000;
//length along x-axis:
length_of_column = 400;
//length along y-axis:
breadth_of_column = 300;

//length of footing, i.e length along x-axis:
length_of_footing = 2500;
//width of footing, i.e length along y-axis:
width_of_footing = 2000;
//Total Depth of footing:
total_depth_of_footing = 500;
//depth of footing at edges:
depth_of_footing_at_edges = 200;

side_clear_cover = 50;
bottom_clear_cover = 75;

dia_rebars_parallel_to_length = 16;
dia_rebars_parallel_to_width = 12;

no_of_rebars_parallel_to_length = 12;
no_of_rebars_parallel_to_width = 10;


!union(){
footing_without_reinforcement(transparent = true);
both_reinforcement();
} // press F5 to see the results

projection_cut(plane = "xy", dist_from_plane = 0, slice = false) {
difference(){
footing_without_reinforcement(transparent = false);
both_reinforcement();
}
}


projection_cut(plane = "xz", dist_from_plane = centers_of_rebars_parallel_to_length[floor(len(centers_of_rebars_parallel_to_length) / 2)], slice = true) {
difference(){
footing_without_reinforcement(transparent = false);
both_reinforcement();
}
}


projection_cut(plane = "yz", dist_from_plane = centers_of_rebars_parallel_to_width[floor(len(centers_of_rebars_parallel_to_width) / 2)], slice = true) {
difference(){
footing_without_reinforcement(transparent = false);
both_reinforcement();
}
}

//// INPUT ENDS HERE ////

//above: assuming all rebars in each direction are spaced evenly and are of same dia.

//function yz_central_rebar () = 
module projection_cut (plane = "xy", dist_from_plane = 0, slice = true){
if (plane == "xy")
{
projection(cut = slice) translate([0,0,-dist_from_plane]) children();
}

else if (plane == "yz")
{
rotate ([0,0,-90]) projection(cut = slice) translate([0,0,-dist_from_plane]) rotate([0,-90,0]) children();
}
else if (plane == "xz" || plane == "zx")
{
rotate ([0,0,180]) projection(cut = slice) translate([0,0,-dist_from_plane]) rotate([90,0,0]) children();
}
}

//calculations:
//upper points of footing:
p1 = [length_of_footing/2, width_of_footing/2, depth_of_footing_at_edges];
p2 = [length_of_footing/2, -width_of_footing/2, depth_of_footing_at_edges];
p3 = [-length_of_footing/2, -width_of_footing/2, depth_of_footing_at_edges];
p4 = [-length_of_footing/2, width_of_footing/2, depth_of_footing_at_edges];
//bottom points of column:
p5 = [length_of_column/2, breadth_of_column/2, total_depth_of_footing];
p6 = [length_of_column/2, -breadth_of_column/2, total_depth_of_footing];
p7 = [-length_of_column/2, -breadth_of_column/2, total_depth_of_footing];
p8 = [-length_of_column/2, breadth_of_column/2,total_depth_of_footing];

module footing_without_reinforcement(transparent = true){


//column:
translate([-length_of_column/2, -breadth_of_column/2, total_depth_of_footing])
cube([length_of_column, breadth_of_column, height_of_column]);

if (transparent == true) %tapered_and_bottom();
else tapered_and_bottom();
module tapered_and_bottom(){
//tapered portion:
polyhedron(
points = [ p1, p2, p3, p4, p5, p6, p7, p8], 
faces = [ [4,5,6],[6,7,4],
[0,1,4], [4,1,5], 
[1,2,5],[5,2,6], 
[2,3,6], [6,3,7], [3,0,7],[7,0,4], 
[0,3,2],[0,2,1],   
]);


//bottom of footing
translate([-length_of_footing/2, -width_of_footing/2, 0])
cube([length_of_footing, width_of_footing, depth_of_footing_at_edges]);
}
}

//reinforcements:
function steps( start, no_of_rebars, end) = [start:(end-start)/(no_of_rebars-1):end];

function height_center_of_rebar_parallel_length () =  length_of_footing >= width_of_footing ? bottom_clear_cover + dia_rebars_parallel_to_length/2 : bottom_clear_cover + dia_rebars_parallel_to_width + dia_rebars_parallel_to_length/2 ;
//echo(height_center_of_rebar_parallel_length());

function height_center_of_rebar_parallel_width () =  length_of_footing >= width_of_footing ? bottom_clear_cover + dia_rebars_parallel_to_length + dia_rebars_parallel_to_width/2 : bottom_clear_cover + dia_rebars_parallel_to_width/2 ;
//echo(height_center_of_rebar_parallel_width());


//reinforcement parallel to length
length_of_rebars_parallel_to_length = length_of_footing - 2 * side_clear_cover;
centers_of_rebars_parallel_to_length = [for (i = steps(-width_of_footing / 2 + side_clear_cover + dia_rebars_parallel_to_length / 2, no_of_rebars_parallel_to_length, width_of_footing / 2 - side_clear_cover - dia_rebars_parallel_to_length / 2)) i]; //a list, may be more useful if rather than calculating arbitrarily, get it from designer!
echo(centers_of_rebars_parallel_to_length);

module reinforcement_parallel_to_length(){
for (i = centers_of_rebars_parallel_to_length) {color("darkred") translate([0,i,height_center_of_rebar_parallel_length ()]) rotate([0,90,0]) cylinder(h = length_of_rebars_parallel_to_length, d = dia_rebars_parallel_to_length, center = true);}
}

//reinforcement_parallel_to_width();

length_of_rebars_parallel_to_width = width_of_footing - 2 * side_clear_cover;
centers_of_rebars_parallel_to_width = [for (i = steps(-length_of_footing / 2 + side_clear_cover + dia_rebars_parallel_to_width / 2, no_of_rebars_parallel_to_width, length_of_footing / 2 - side_clear_cover - dia_rebars_parallel_to_width / 2)) i]; // a list
echo(centers_of_rebars_parallel_to_width);

module reinforcement_parallel_to_width(){
for (i = centers_of_rebars_parallel_to_width) {color("darkred") translate([i,0,height_center_of_rebar_parallel_width ()]) rotate([90,0,0]) cylinder(h = length_of_rebars_parallel_to_width, d = dia_rebars_parallel_to_width, center = true);}
}

//reinforcement_parallel_to_width();

module both_reinforcement(){reinforcement_parallel_to_length(); reinforcement_parallel_to_width();}